# OpenML dataset: LastFM_dataset

https://www.openml.org/d/44199

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Last FM Dataset

-------
Version
-------

Version 1.0 (May 2011)

-----------
Description
-----------

    This dataset contains social networking, tagging, and music artist listening information 
    from a set of 2K users from Last.fm online music system.
    http://www.last.fm 

    The dataset is released in the framework of the 2nd International Workshop on 
    Information Heterogeneity and Fusion in Recommender Systems (HetRec 2011) 
    http://ir.ii.uam.es/hetrec2011 
    at the 5th ACM Conference on Recommender Systems (RecSys 2011)
    http://recsys.acm.org/2011 

------- 
License
-------

   The users' names and other personal information in Last.fm are not provided in the dataset.

   The data contained in hetrec2011-lastfm-2k.zip is made available for non-commercial use.
   
   Those interested in using the data in a commercial context should contact Last.fm staff:
   http://www.lastfm.com/about/contact

----------------
Acknowledgements
----------------

   This work was supported by the Spanish Ministry of Science and Innovation (TIN2008-06566-C04-02), 
   and the Regional Government of Madrid (S2009TIC-1542).

----------
References
----------

   When using this dataset you should cite:
      - Last.fm website, http://www.lastfm.com

   You may also cite HetRec'11 workshop as follows:

   @inproceedings{Cantador:RecSys2011,
      author = {Cantador, Iv\'{a}n and Brusilovsky, Peter and Kuflik, Tsvi},
      title = {2nd Workshop on Information Heterogeneity and Fusion in Recommender Systems (HetRec 2011)},
      booktitle = {Proceedings of the 5th ACM conference on Recommender systems},
      series = {RecSys 2011},
      year = {2011},
      location = {Chicago, IL, USA},
      publisher = {ACM},
      address = {New York, NY, USA},
      keywords = {information heterogeneity, information integration, recommender systems},
   }

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44199) of an [OpenML dataset](https://www.openml.org/d/44199). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44199/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44199/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44199/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

